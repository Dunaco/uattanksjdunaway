﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIAimAhead : MonoBehaviour
{
    // Declare variables
    public TankData tankData;
    public motor motor;
    public Transform target;
    public GameObject bullet;
    public float firingDelay = 2.0f;
    private float nextEventTime;
    public GameObject bulletSpawn;
    private Transform tf;
    private int avoidanceStage = 0;
    public float avoidanceTime = 2.0f;
    private float exitTime;
    public enum AIState { Chase, ChaseAndFire, CheckForFlee, Flee, Rest}
    public AIState aiState = AIState.Chase;
    public float fleeDistance = 1.0f;
    public float stateEnterTime;
    public float aiSenseRadius = 5.0f;
    public float restingHealRate = 10f;



    private void Awake()
    {
        // Get's the AI's transform
        tf = GetComponent<Transform>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (tankData == null)
        {
            tankData = GetComponent<TankData>();
        }
        if (motor == null)
        {
            motor = GetComponent<motor>();
        }
    }

    public void DoChase()
    {
        // Rotate towards Target
        motor.RotateTowards(target.position, tankData.turnSpeed);
        if (CanMove(tankData.forwardSpeed))
        {
            // Go Forward
            motor.Move(tankData.forwardSpeed);
        }
        else
        {
            avoidanceStage = 1;
        }
    }

    public void DoFlee()
    {
        Vector3 distanceToTarget = target.position - tf.position;

        Vector3 distanceAwayTarget = -1 * distanceToTarget;

        distanceAwayTarget.Normalize();

        distanceAwayTarget *= fleeDistance;

        Vector3 fleePosition = distanceAwayTarget + tf.position;
        motor.RotateTowards(fleePosition, tankData.turnSpeed);
        motor.Move(tankData.forwardSpeed);
    }

    bool CanMove(float speed)
    {
        // Casts a raycast to see if we hit something
        RaycastHit hit;
        if (Physics.Raycast(tf.position, tf.forward, out hit, speed - 0.25f))
        {

            if (!hit.collider.CompareTag("Player"))
            {

                return false;
            }
        }
        return true;
    }

    public void CheckForFlee()
    {
        // TODO: Write CheckForFlee
    }

    public void DoRest()
    {
        tankData.health += restingHealRate * Time.deltaTime;

        // Increases health over time
        tankData.health = Mathf.Min(tankData.health, tankData.maxHealth);
    }

    public void ChangeState(AIState newState)
    {
        // Changes State to the new State
        aiState = newState;

        // Save the time that we switched states
        stateEnterTime = Time.time;
    }

    void DoAvoidance()
    {
        if (avoidanceStage == 1)
        {
            // rotate left
            motor.Rotate(-1 * tankData.turnSpeed * Time.deltaTime);

            if (CanMove(tankData.forwardSpeed))
            {
                avoidanceStage = 2;
                // Set the number of seconds we'll stay in Stage2
                exitTime = avoidanceTime;
            }
        }
        else if (avoidanceStage == 2)
        {
            // Check to see if we can go forward
            if (CanMove(tankData.forwardSpeed))
            {
                // Subtracts the time to leave the Avoidance mode
                exitTime -= Time.deltaTime;
                motor.Move(tankData.forwardSpeed);

                // If the exit time is out, return to previous state
                if (exitTime <= 0)
                {
                    avoidanceStage = 0;
                }
            }
            else
            {
                // Can't move forward, reset progress
                avoidanceStage = 1;
            }
        }
    }

    public void FireBullet()
    {
        //Instantiate the Bullet and store it in an object
        GameObject bulletShell = Instantiate(bullet, bulletSpawn.transform.position, transform.rotation);
        // Sends the Damage modifier to the BulletBehavior class.
        BulletBehavior bulletDamage = bulletShell.GetComponent<BulletBehavior>();
        bulletDamage.AssignDamageAmount(tankData.attackDamage);
        // Resets the delay
        nextEventTime = Time.time + firingDelay;
    }

    public bool CanHear()
    {
        if (Vector3.Distance(target.position, tf.position) <= aiSenseRadius)
        {
            return true;
        }
        return false;
    }

    // Update is called once per frame
    void Update()
    {

        if (aiState == AIState.Chase)
        {
            //Check for Avoidance Stages
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();
            }

            // Check for Transitions
            if (tankData.health < tankData.maxHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (CanHear())
            {
                ChangeState(AIState.ChaseAndFire);
            }
        }
        else if (aiState == AIState.ChaseAndFire)
        {
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoChase();

                // Firing delay for the AI
                if (Time.time >= nextEventTime)
                {
                    // Calls Function to Fire rounds
                    FireBullet();
                }
            }
            // Transition Check
            if (tankData.health < tankData.maxHealth * 0.5f)
            {
                ChangeState(AIState.CheckForFlee);
            }
            else if (CanHear() == false)
            {
                ChangeState(AIState.Chase);
            }
        }
        else if (aiState == AIState.Flee)
        {
            // Perform Behaviors
            if (avoidanceStage != 0)
            {
                DoAvoidance();
            }
            else
            {
                DoFlee();
            }

            // Check for Transitions
            if (Time.time >= stateEnterTime + 5)
            {
                ChangeState(AIState.CheckForFlee);
            }
        }
        else if (aiState == AIState.CheckForFlee)
        {
            // Perform Behaviors
            CheckForFlee();

            // Check for Transitions
            if (CanHear())
            {
                ChangeState(AIState.Flee);
            }
            else
            {
                ChangeState(AIState.Rest);
            }
        }
        else if (aiState == AIState.Rest)
        {
            // Perform Behaviors
            DoRest();

            // Check for Transitions
            if (CanHear())
            {
                ChangeState(AIState.Flee);
            }
            else if (tankData.health >= tankData.maxHealth)
            {
                    ChangeState(AIState.Chase);
            }
        }
    }
}

