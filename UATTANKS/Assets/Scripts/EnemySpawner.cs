﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public float spawnDelay = 15.0f;
    public float spawnTimer = 0.0f;
    public GameObject[] enemyTank;

    // Start is called before the first frame update
    void Start()
    {
        // Creates a list of enemy spawners
        GameManager.instance.spawnList.Add(this);
        
    }

    private void OnDestroy()
    {
        // removes a list of spawners
        GameManager.instance.spawnList.Remove(this);
    }

    // Function to choose which enemy spawns
    public GameObject ChooseEnemy()
    {
        // Random number between 1-4
        int randomNumber = Random.Range(0, 4);
        if (randomNumber <= 1)
        {
            return enemyTank[0];
        }
        else if (randomNumber <= 2 && randomNumber > 1)
        {
            return enemyTank[1];
        }
        else if (randomNumber <= 3 && randomNumber > 2)
        {
            return enemyTank[2];
        }
        else
        {
            return enemyTank[3];
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (spawnTimer <= 0)
        {
            // Picks a random index of spawn
            int randomIndex = Random.Range(0, GameManager.instance.spawnList.Count);

            // Grabs a spawnpoint from list
            Transform spawnPoint = GameManager.instance.spawnList[randomIndex].transform;

            // Spawn at that point.
            GameObject spawnedEnemy = Instantiate(ChooseEnemy(), spawnPoint.position, spawnPoint.rotation);
            Destroy(this);
            spawnTimer = spawnDelay;

        }
    }
}
