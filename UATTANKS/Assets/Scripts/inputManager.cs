﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputManager : MonoBehaviour
{
    // Declare Variables.
    public TankData tankData;
    public motor motor;
    public GameObject bullet;
    private float nextEventTime;
    public GameObject bulletSpawn;
    public bool isDead = true;
    public bool isPlayerOne = true;
    public bool isPlayerTwo = false;

    private void OnDestroy()
    {
        isDead = true;
    }

    // Start is called before the first frame update
    void Start()
    {
        // Set's the first delay for the timer.
        nextEventTime = Time.time + tankData.firingDelay;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerOne)
        {
            // If W is held
            if (Input.GetKey(KeyCode.W))
            {
                // Calls the function from motor class to move forward, Passes the speed.
                motor.Move(tankData.forwardSpeed);
            }
            // If S is held
            else if (Input.GetKey(KeyCode.S))
            {
                // Sends the data to go backwards
                motor.Move(tankData.reverseSpeed * -1);
            }
            // If D is held
            if (Input.GetKey(KeyCode.D))
            {
                // Sends data to turn Right
                motor.Rotate(tankData.turnSpeed);
            }
            // If A is held
            if (Input.GetKey(KeyCode.A))
            {
                // Sends data to turn Left
                motor.Rotate(tankData.turnSpeed * -1);

            }
            // If Space is pressed
            if (Input.GetKeyDown(KeyCode.Space))
            {
                // Checks to see if timer is up
                if (Time.time >= nextEventTime)
                {

                    //Instantiate the Bullet and store it in an object

                    GameObject bulletShell = Instantiate(bullet, bulletSpawn.transform.position, transform.rotation);
                    // Sends damage to the Bullet class
                    BulletBehavior bulletDamage = bulletShell.GetComponent<BulletBehavior>();
                    bulletDamage.AssignDamageAmount(tankData.attackDamage);
                    // resets timer
                    nextEventTime = Time.time + tankData.firingDelay;
                }
            }
        }

        if (isPlayerTwo)
        {
            // If Up arrow is held
            if (Input.GetKey(KeyCode.UpArrow))
            {
                // Calls the function from motor class to move forward, Passes the speed.
                motor.Move(tankData.forwardSpeed);
            }
            // If down arrow is held
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                // Sends the data to go backwards
                motor.Move(tankData.reverseSpeed * -1);
            }
            // If right arrow is held
            if (Input.GetKey(KeyCode.RightArrow))
            {
                // Sends data to turn Right
                motor.Rotate(tankData.turnSpeed);
            }
            // If left arrow is held
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                // Sends data to turn Left
                motor.Rotate(tankData.turnSpeed * -1);

            }
            // If RightControl is pressed
            if (Input.GetKeyDown(KeyCode.RightControl))
            {
                // Checks to see if timer is up
                if (Time.time >= nextEventTime)
                {

                    //Instantiate the Bullet and store it in an object

                    GameObject bulletShell = Instantiate(bullet, bulletSpawn.transform.position, transform.rotation);
                    // Sends damage to the Bullet class
                    BulletBehavior bulletDamage = bulletShell.GetComponent<BulletBehavior>();
                    bulletDamage.AssignDamageAmount(tankData.attackDamage);
                    // resets timer
                    nextEventTime = Time.time + tankData.firingDelay;
                }
            }
        }

        // If Espace is pressed
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            // TODO : Bring up menu
            Debug.Log("Bringing up Menu");
        }
    }
}
