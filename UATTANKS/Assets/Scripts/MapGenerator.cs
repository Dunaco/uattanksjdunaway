﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGenerator : MonoBehaviour
{
    public int rows;
    public int cols;
    public int mapSeed;

    public bool isMapOfTheDay = false;
    public bool isMapRandom = true;

    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    public GameObject[] gridPrefabs;
    private Room[,] grid;


    // Start is called before the first frame update
    void Start()
    {
        
        // Sets the seeds for the map of day or random based off of time
        if (isMapOfTheDay)
        {
            mapSeed = DateToInt(DateTime.Now.Date);
        }
        else if (isMapRandom)
        {
            mapSeed = DateToInt(DateTime.Now);
        }



        // Generate the grid
        GenerateGrid();


    }

    public void GenerateGrid()
    {
        UnityEngine.Random.InitState(mapSeed);

        // Clears the grid, x is columns and y is rows
        grid = new Room[cols, rows];

        // For Each row in the grid
        for (int i = 0; i < rows; i++)
        {
            // For each Column in the row
            for (int j = 0; j < cols; j++)
            {
                // Find Location
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                // Create a new grid in the location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

                // Set it as the parent
                tempRoomObj.transform.parent = this.transform;

                // Give it a name
                tempRoomObj.name = "Room_" + j + "," + i;

                // Grab the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();
               
                //Open doors
                // If on bottom row, open doors to the north
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (i == rows-1)
                {
                    // if on top row, open south doors
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    // Otherwise both North and South doors open
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                // if on first column, open east door
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (j == rows - 1)
                {
                    // if on last column, open west door
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    // Open both west and east doors
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                // Saves it to the grid array
                grid[j, i] = tempRoom;



            }
        }
    }

    public int DateToInt(DateTime dateToUse)
    {
        // Add up our date and return it
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    // returns a random Room
    public GameObject RandomRoomPrefab ()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
