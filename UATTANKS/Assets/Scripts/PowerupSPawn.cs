﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupSPawn : MonoBehaviour
{
    public float spawnDelay = 15.0f;
    public float spawnTimer = 0.0f;
    public GameObject[] powerUpPrefab;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.PowerupSpawnlist.Add(this);
    }

    private void OnDestroy()
    {
        GameManager.instance.PowerupSpawnlist.Remove(this);
    }

    public GameObject ChoosePowerup()
    {
        int randomNumber = Random.Range(0, 4);
        if (randomNumber <= 1)
        {
            return powerUpPrefab[0];
        }
        else if (randomNumber <= 2 && randomNumber > 1)
        {
            return powerUpPrefab[1];
        }
        else if (randomNumber <= 3 && randomNumber > 2)
        {
            return powerUpPrefab[2];
        }
        else
        {
            return powerUpPrefab[3];
        }

    }

    // Update is called once per frame
    void Update()
    {
     

        
    }
}
