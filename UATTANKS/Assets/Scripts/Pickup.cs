﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public Powerup powerup;
    public AudioClip feedback;
    private Transform tf;
    public float spawnDelay = 10;
    private GameObject spawnedPickedup;
    private float nextSpawnTime;
    private GameObject spawnedPickup;
    public GameObject Powerup1;
    public GameObject Powerup2;
    public GameObject Powerup3;
    public GameObject Powerup4;



    // Start is called before the first frame update
    void Start()
    {
        // Grabs the Transform
        tf = gameObject.GetComponent<Transform>();
        // Sets spawn delay
        nextSpawnTime = Time.time + spawnDelay;
    }

    // Function to select powerup
    public GameObject ChoosePowerup()
    {
        // Sets it to a random number out of 40
        int randomNumber = Random.Range(0, 40);
        if (randomNumber <= 10)
        {
            // Increases firerate for 15 seconds
            powerup.fireRateModifier = 50;
            powerup.duration = 15;
            return Powerup1;
        }

        else if (randomNumber <= 20 && randomNumber > 10)
        {
            // Increases all abilities for 10 seconds
            powerup.fireRateModifier = 50;
            powerup.maxHealthModifier = 2000;
            powerup.healthModifier = 2000;
            powerup.speedModifier = 15;
            powerup.duration = 10;
            return Powerup2;
        }
        else if (randomNumber <= 30 && randomNumber > 20)
        {
            // Increases max health pernamently
            powerup.maxHealthModifier = 100;
            powerup.healthModifier = 100;
            powerup.isPermanent = true; 
            return Powerup3;
        }
        else
        {
            // Increases speed pernamently
            powerup.speedModifier = 10;
            powerup.isPermanent = true;
            return Powerup4;
        }

    }

    // Checks for collision with tank
    private void OnTriggerEnter(Collider other)
    {
        // Stores the other object's PowerupController, should it have one
        PowerupController powCon = other.GetComponent<PowerupController>();

        // if the PowerupController is found
        if(powCon != null)
        {
            // Add powerup to list
            powCon.Add(powerup);

            // Play Audio Cue
            if (feedback != null)
            {
                AudioSource.PlayClipAtPoint(feedback, tf.position, 1.0f);
            }

            // Destroy the pickup object
            Destroy(gameObject);
        }
    }

 

    // Update is called once per frame
    void Update()
    {
        // if nothing is spawned
        if (spawnedPickup == null)
        {
            // Check to see if spawn time is up
            if (Time.time > nextSpawnTime)
            {
                // Spawn powerup
                spawnedPickedup = Instantiate(ChoosePowerup(), tf.position, Quaternion.identity) as GameObject;
                nextSpawnTime = Time.time + spawnDelay;
            }
        }
        else
        {
            // postpone spawn time
            nextSpawnTime = Time.time + spawnDelay;
        }
    }
}
