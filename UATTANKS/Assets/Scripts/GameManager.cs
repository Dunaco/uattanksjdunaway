﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GameManager : MonoBehaviour
{
    // Creates the static variable for the GameManager
    public static GameManager instance;
    // Variable to hold the score
    private float score = 0;

    public int rows;
    public int cols;
    public int mapSeed;
    public GameObject PlayerTank;
    public GameObject PlayerTank1;
    public GameObject PlayerTank2;
    public inputManager inputManager;
    private int playersSelected;

    public CanvasGroup canvasGroup;
    public GameObject panel;
    public GameObject startButton;
    public GameObject SplitButton;
    public GameObject OptionBut;
    public GameObject MapOfDayButton;
    public GameObject exitButton;

    public bool isMapOfTheDay = false;
    public bool isMapRandom = true;

    public List<PlayerSpawn> PlayerSpawnList = new List<PlayerSpawn>();
    public List<EnemySpawner> spawnList = new List<EnemySpawner > ();
    public List<PowerupSPawn> PowerupSpawnlist = new List<PowerupSPawn>();
    private float roomWidth = 50.0f;
    private float roomHeight = 50.0f;
    public GameObject[] gridPrefabs;
    private Room[,] grid;


   

    //Always running
    private void Awake ()
    {
        // Checks to see if there is a GameManager instance or not
        if (instance == null)
        {
            // Creates the instance
            instance = this;
        }
        else
        {
            // Displays ERROR for there are more than one GameManager
            Debug.LogError("ERROR: There can only be one GameManager.");
            // Destroys the current GameManager since there can only be one.
            Destroy(gameObject);
        }
    }


    
    public void StartGame()
    {
        playersSelected = 1;
        isMapOfTheDay = false;
        isMapRandom = true;
        DisableStartmenu();
        GenerateGrid();

    }

    public void StartSplit()
    {
        playersSelected = 2;
        isMapRandom = true;
        isMapOfTheDay = false;
        DisableStartmenu();
        GenerateGrid();
    }

    public void StartMapOfDay()
    {
        playersSelected = 1;
        isMapOfTheDay = true;
        isMapRandom = false;
        DisableStartmenu();
        GenerateGrid();
    }

    public void DisableStartmenu()
    {
        panel.SetActive(false);
        startButton.SetActive(false);
        SplitButton.SetActive(false);
        OptionBut.SetActive(false);
        MapOfDayButton.SetActive(false);
        exitButton.SetActive(false);
    }

    public void show()
    {
        panel.SetActive(true);
        startButton.SetActive(true);
        SplitButton.SetActive(true);
        OptionBut.SetActive(true);
        MapOfDayButton.SetActive(true);
        exitButton.SetActive(true);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    // Start is called before the first frame update
    void Start()
    {

    }



    public void GenerateGrid()
    {
        UnityEngine.Random.InitState(mapSeed);

        // Clears the grid, x is columns and y is rows
        grid = new Room[cols, rows];

        // For Each row in the grid
        for (int i = 0; i < rows; i++)
        {
            // For each Column in the row
            for (int j = 0; j < cols; j++)
            {
                // Find Location
                float xPosition = roomWidth * j;
                float zPosition = roomHeight * i;
                Vector3 newPosition = new Vector3(xPosition, 0.0f, zPosition);

                // Create a new grid in the location
                GameObject tempRoomObj = Instantiate(RandomRoomPrefab(), newPosition, Quaternion.identity) as GameObject;

                // Set it as the parent
                tempRoomObj.transform.parent = this.transform;

                // Give it a name
                tempRoomObj.name = "Room_" + j + "," + i;

                // Grab the room object
                Room tempRoom = tempRoomObj.GetComponent<Room>();

                //Open doors
                // If on bottom row, open doors to the north
                if (i == 0)
                {
                    tempRoom.doorNorth.SetActive(false);
                }
                else if (i == rows - 1)
                {
                    // if on top row, open south doors
                    tempRoom.doorSouth.SetActive(false);
                }
                else
                {
                    // Otherwise both North and South doors open
                    tempRoom.doorNorth.SetActive(false);
                    tempRoom.doorSouth.SetActive(false);
                }
                // if on first column, open east door
                if (j == 0)
                {
                    tempRoom.doorEast.SetActive(false);
                }
                else if (j == rows - 1)
                {
                    // if on last column, open west door
                    tempRoom.doorWest.SetActive(false);
                }
                else
                {
                    // Open both west and east doors
                    tempRoom.doorEast.SetActive(false);
                    tempRoom.doorWest.SetActive(false);
                }

                // Saves it to the grid array
                grid[j, i] = tempRoom;



            }
        }
    }

    public int DateToInt(DateTime dateToUse)
    {
        // Add up our date and return it
        return dateToUse.Year + dateToUse.Month + dateToUse.Day + dateToUse.Hour + dateToUse.Minute + dateToUse.Second + dateToUse.Millisecond;
    }

    // returns a random Room
    public GameObject RandomRoomPrefab()
    {
        return gridPrefabs[UnityEngine.Random.Range(0, gridPrefabs.Length)];
    }


    // Function to Add the score since the score variable is private.
    public void AddScore(float pointsToAdd)
    {
        // Adds the score
        score += pointsToAdd;
    }



    // Update is called once per frame
    void Update()
    {
        if (inputManager.isDead)
        {
            if (playersSelected == 2)
            {
                // Picks a random index of spawn
                int randomIndex = UnityEngine.Random.Range(0, GameManager.instance.PlayerSpawnList.Count);

                // Grabs a spawnpoint from list
                Transform spawnPoint = GameManager.instance.PlayerSpawnList[randomIndex].transform;

                // Spawn at that point.
                GameObject spawnedPlayer = Instantiate(PlayerTank1, spawnPoint.position, spawnPoint.rotation);
                // Picks a random index of spawn
                int randomIndex2 = UnityEngine.Random.Range(0, GameManager.instance.PlayerSpawnList.Count);

                // Grabs a spawnpoint from list
                Transform spawnPoint2 = GameManager.instance.PlayerSpawnList[randomIndex2].transform;

                // Spawn at that point.
                GameObject spawnedPlayer2 = Instantiate(PlayerTank2, spawnPoint2.position, spawnPoint2.rotation);
            }
            else
            {


                // Picks a random index of spawn
                int randomIndex = UnityEngine.Random.Range(0, GameManager.instance.PlayerSpawnList.Count);

                // Grabs a spawnpoint from list
                Transform spawnPoint = GameManager.instance.PlayerSpawnList[randomIndex].transform;

                // Spawn at that point.
                GameObject spawnedPlayer = Instantiate(PlayerTank, spawnPoint.position, spawnPoint.rotation);
            }
        }

       
    }
}
