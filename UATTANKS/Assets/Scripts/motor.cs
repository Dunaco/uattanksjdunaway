﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class motor : MonoBehaviour
{
    // Holds the CharacterController for the object.
    private CharacterController characterController;
    // Holds the Transform for the object
    private Transform tf;

    // Start is called before the first frame update
    public void Awake()
    {
        // Grabs the Transform Component
        tf = gameObject.GetComponent<Transform>();
    }
    void Start()
    {
        // Pulls the CharacterController Component and assigns it to the variable to get access to it's functions
        characterController = gameObject.GetComponent<CharacterController>();
        
    }

    // Function to Move the GameObjects
    public void Move(float moveSpeed)
    {
        // Creates a vector to transfer the speed data and forces it in the direction of the Object
        Vector3 speedVector = tf.forward;

        // Calls SimpleMove() to move our Object and passes the Vector after giving it our speed value.
        characterController.SimpleMove(speedVector * moveSpeed);
    }

    // Function to Rotate the GameObjects
    public void Rotate(float turnSpeed)
    {
        // Creates a vector to hold our rotation speed and sets the direction of the vector
        Vector3 rotateVector = Vector3.up;

        // Rotates the Object in local space, according to the value we assigned.
        tf.Rotate(rotateVector * turnSpeed, Space.Self);
    }

    public bool RotateTowards(Vector3 target, float speed)
    {
        Vector3 distanceBetweenTarget;

        distanceBetweenTarget = target - tf.position;

        Quaternion targetRotation = Quaternion.LookRotation(distanceBetweenTarget);
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);

        if (tf.rotation == targetRotation)
        {
            return false;
        }
        tf.rotation = Quaternion.RotateTowards(tf.rotation, targetRotation, speed * Time.deltaTime);

        return true;
    }

    // Update is called once per frame
    void Update()
    {

    }
}
