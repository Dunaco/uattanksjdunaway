﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        // Adds all spawns to a list
        GameManager.instance.PlayerSpawnList.Add(this);
    }

    // Update is called once per frame
    void Update()
    {
        // removes spawn from list
        GameManager.instance.PlayerSpawnList.Remove(this);
    }
}
