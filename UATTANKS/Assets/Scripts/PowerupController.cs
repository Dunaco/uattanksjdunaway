﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{
    public List<Powerup> powerups;
    public TankData data;
 
    // Start is called before the first frame update
    void Start()
    {
        // Creates a list for powerups
        powerups = new List<Powerup>();
    }

    public void Add(Powerup powerup)
    {
        // Activates the powerup
        powerup.OnActivate(data);

        // adds non-pernament powerups to a list to be removed later
        if (!powerup.isPermanent)
        {
            powerups.Add(powerup);
        }
    }

    


    // Update is called once per frame
    void Update()
    {
        // Creates a List to hold expired Powerups
        List<Powerup> expiredPowerups = new List<Powerup>();

        // Goes through all the powerups in our list
        foreach (Powerup power in powerups)
        {
            // Reduces the timer
            power.duration -= Time.deltaTime;

            // Adds to list of expired powerups if timer run out
            if (power.duration <= 0)
            {
                expiredPowerups.Add(power);
            }
        }

        // Utilize the expired list to remove powerups
        foreach(Powerup power in expiredPowerups)
        {
            // Decactivates powerup and removes it from the list.
            power.OnDeactivate(data);
            powerups.Remove(power);
        }
        // Clears the expired list before it leaves function
        expiredPowerups.Clear();
    }
}
