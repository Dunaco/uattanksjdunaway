﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class TankData : MonoBehaviour
{
    // Declare Variables
    public float forwardSpeed = 2;
    public float reverseSpeed = 0.5f;
    public float turnSpeed = .5f;
    public float maxHealth = 200;
    public float health;
    public float attackDamage = 50;
    public GameManager Game;
    public float pointsOnDeath = 100;
    public float firingDelay = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        // sets health to Max Health
        health = maxHealth;
        
    }



    // Function to take damage after collision
    public void TakeDamage (float damageDealt)
    {
        // Takes the damage passed by the bullet class and subtracts it
        health -= damageDealt;
    }

    // Update is called once per frame
    void Update()
    {
        // Checks to make sure health doesn't go above max
        if (health > maxHealth)
        {
            // if over max, set to max
            health = maxHealth;
        }

        // Checks to see if you have any health left
        if (health <= 0)
        {
            // Out of health, destroys the object.
            Destroy(this.gameObject);
            // Sends the score value to the GameManager.
            Game.AddScore(pointsOnDeath);
        }
    }
}
