﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class Powerup 
{
    public float speedModifier;
    public float healthModifier;
    public float maxHealthModifier;
    public float fireRateModifier;

    public float duration;
    public bool isPermanent;


    public void OnActivate(TankData target)
    {
        // Adds the modifiers to the abilites
        target.forwardSpeed += speedModifier;
        target.health += healthModifier;
        target.maxHealth += maxHealthModifier;
        target.firingDelay -= fireRateModifier;
    }

    public void OnDeactivate(TankData target)
    {
        // Removes the modifiers from abilities
        target.forwardSpeed -= speedModifier;
        target.health -= healthModifier;
        target.maxHealth -= maxHealthModifier;
        target.firingDelay += fireRateModifier;
    }
}
