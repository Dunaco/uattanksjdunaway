﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    // Holds motor class
    public motor motor;
    // Variable for bulletSpeed, adjustable
    public float bulletSpeed = 5;
    // Holds TankData class
    public TankData tankData;
    // variable for damage
    private float damage;
    public float bulletStayingTime = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Function to assign damage to the GameObject
    public void AssignDamageAmount(float damageAmmount)
    {
        // Transfers the damage amount from the other class to a variable in this class
        damage = damageAmmount;
    }

    // When bullet comes in contact with another collider, this activates
    private void OnTriggerEnter (Collider other)
    {
        // Destroys the bullet
        Destroy(this.gameObject);
        // Assigns damage to the recieving Object.
        other.gameObject.GetComponent<TankData>().TakeDamage(damage);
    }

    // Update is called once per frame
    void Update()
    {
        // Speed of bullet, and moves bullet foward
        motor.Move(bulletSpeed);
        // Destroys bullet after timer is out. 
        Destroy(this.gameObject, bulletStayingTime);

    }
}
