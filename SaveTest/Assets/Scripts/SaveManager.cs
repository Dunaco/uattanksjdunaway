﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    public Text theText;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Save()
    {
        PlayerPrefs.SetString("TextData", theText.text);
        PlayerPrefs.Save();
    }

    public void Load()
    {
        theText.text = PlayerPrefs.GetString("TextData");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
